((factory) ->
    if typeof define is 'function' and define.amd
        define(['jquery'], factory)
    else
        factory(jQuery)
)($) ->
    json_to_form = (json, options) ->
        options = $.extend {}, $.fn.json2form.defaults, options
        @.each (index, item) ->
            for attribute, value of json
                fields = $("[name=#{attribute}]", @)
                if fields.length > 0
                    fields.each (index, field) ->
                        field = $ field
                        switch field.prop('type')
                            when 'text', 'textarea', 'select-one', 'select-multiple'
                                field.val value.toString()
                            when 'checkbox', 'radio'
                                if value.toString() is field.val().toString() then field.prop 'checked', true
                                else field[0].checked = false
                        field.trigger 'change'
                        return
            return
    $.fn.json2form.defaults = {}
    $.fn.json_to_form = json_to_form
    return
