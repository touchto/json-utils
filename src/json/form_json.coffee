rNull = /^\s*$/
rBool = /^(true|false)$/i

buildValue = (sValue) ->
    if rNull.test(sValue) then return null
    if rBool.test(sValue) then return sValue.toLowerCase() is 'true'
    if isFinite(sValue) then return parseFloat sValue
    if isFinite(Date.parse(sValue)) then return new Date sValue
    sValue

form_to_json = (querystring) ->
    params = querystring.split('&')
    stack = {}
    for param in params
        param = param.split('=')
        attribute = param[0]
        value = buildValue(unescape(param[1]))

        if attribute.match(/(.*?)\[(.*?)\]/)
            attribute = RegExp.$1
            attribute2 = RegExp.$2
            if attribute2
                stack[attribute] = {} unless attribute of stack
                stack[attribute][attribute2] = value
            else
                stack[attribute] = [] unless attribute of stack
                stack[attribute].push value
        else
            stack[attribute] = value
    stack

if typeof define is 'function' and define.amd
    define 'form_json', [], ->
        return form_to_json
else
    window.form_json = form_to_json
