show_model_errors = (model, errors) ->
    @$('.control-group').removeClass('error')
    @$('.help-inline').text('')
    _.forEach errors, (error) ->
        field = @$("[name=#{error.name}]")
        field.parents('.form-group').addClass('has-error')
        field.parents('.form-group').find('.help-block').text(error.message)
        return
    alert('Please correct the errors above')
    return

if typeof define is 'function' and define.amd
    define 'showerrors', ['jquery', 'underscore'], ($, _) ->
        return show_model_errors
else
    window.showErrors = show_model_errors
