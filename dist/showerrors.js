(function() {
  var show_model_errors;

  show_model_errors = function(model, errors) {
    this.$('.control-group').removeClass('error');
    this.$('.help-inline').text('');
    _.forEach(errors, function(error) {
      var field;
      field = this.$("[name=" + error.name + "]");
      field.parents('.form-group').addClass('has-error');
      field.parents('.form-group').find('.help-block').text(error.message);
    });
    alert('Please correct the errors above');
  };

  if (typeof define === 'function' && define.amd) {
    define('showerrors', ['jquery', 'underscore'], function($, _) {
      return show_model_errors;
    });
  } else {
    window.showErrors = show_model_errors;
  }

}).call(this);
