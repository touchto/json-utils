(function() {
  (function(factory) {
    if (typeof define === 'function' && define.amd) {
      return define(['jquery'], factory);
    } else {
      return factory(jQuery);
    }
  })($)(function() {
    var json_to_form;
    json_to_form = function(json, options) {
      options = $.extend({}, $.fn.json2form.defaults, options);
      return this.each(function(index, item) {
        var attribute, fields, value;
        for (attribute in json) {
          value = json[attribute];
          fields = $("[name=" + attribute + "]", this);
          if (fields.length > 0) {
            fields.each(function(index, field) {
              field = $(field);
              switch (field.prop('type')) {
                case 'text':
                case 'textarea':
                case 'select-one':
                case 'select-multiple':
                  field.val(value.toString());
                  break;
                case 'checkbox':
                case 'radio':
                  if (value.toString() === field.val().toString()) {
                    field.prop('checked', true);
                  } else {
                    field[0].checked = false;
                  }
              }
              field.trigger('change');
            });
          }
        }
      });
    };
    $.fn.json2form.defaults = {};
    $.fn.json_to_form = json_to_form;
  });

}).call(this);
