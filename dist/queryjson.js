(function() {
  define(function() {
    var buildValue, queryJSON, rBool, rNull;
    rNull = /^\s*$/;
    rBool = /^(true|false)$/i;
    buildValue = function(sValue) {
      if (rNull.test(sValue)) {
        return null;
      }
      if (rBool.test(sValue)) {
        return sValue.toLowerCase() === 'true';
      }
      if (isFinite(sValue)) {
        return parseFloat(sValue);
      }
      if (isFinite(Date.parse(sValue))) {
        return new Date(sValue);
      }
      return sValue;
    };
    queryJSON = function(querystring) {
      var attribute, attribute2, param, params, stack, value, _i, _len;
      params = querystring.split('&');
      stack = {};
      for (_i = 0, _len = params.length; _i < _len; _i++) {
        param = params[_i];
        param = param.split('=');
        attribute = param[0];
        value = buildValue(unescape(param[1]));
        if (attribute.match(/(.*?)\[(.*?)]/)) {
          attribute = RegExp.$1;
          attribute2 = RegExp.$2;
          if (attribute2) {
            if (!(attribute in stack)) {
              stack[attribute] = {};
            }
            stack[attribute][attribute2] = value;
          } else {
            if (!(attribute in stack)) {
              stack[attribute] = [];
            }
            stack[attribute].push(value);
          }
        } else {
          stack[attribute] = value;
        }
      }
      return stack;
    };
    return queryJSON;
  });

}).call(this);
