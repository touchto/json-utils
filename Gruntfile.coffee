module.exports = (grunt) ->
    server = null
    coffee_files = grunt.file.expandMapping ['src/**/*.coffee'], 'dist/',
        flatten: true
        rename: (dist_base, dist_path) ->
            dist_base + dist_path.replace(/\.coffee$/, '.js')
    grunt.initConfig
        pkg: '<json:package.json>'
        watch:
            coffee:
                files: ['src/**/*.coffee']
                task: 'coffee:compile'
        coffee:
            options:
                bare: false
            compile:
                files: coffee_files

    grunt.loadNpmTasks 'grunt-contrib-watch'
    grunt.loadNpmTasks 'grunt-contrib-coffee'
    grunt.registerTask 'default', 'coffee:compile'
    return
